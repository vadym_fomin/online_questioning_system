package builder;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

import models.dto.*;
import play.data.DynamicForm;
import play.libs.Json;
import utils.Constant;

/**
 * This class allows to get the fieldDTO instance from dynamic form
 */
public class FieldBuilder {

    private DynamicForm requestParameters;

    public FieldBuilder(DynamicForm requestParameters) {
        this.requestParameters = requestParameters;
    }

    public FieldDTO getFieldDTOFromRequest() {
        FieldDTO field = new FieldDTO();
        String label = requestParameters.get(Constant.LABEL);
        String type = requestParameters.get(Constant.TYPE);
        String realType = requestParameters.get(Constant.REAL_TYPE);
        Double importance = requestParameters.get(Constant.IMPORTANCE)!=null?Double.parseDouble(requestParameters.get(Constant.IMPORTANCE)):null;
        String extremum = requestParameters.get(Constant.EXTREMUM);
        field.setLabel(label);
        field.setFieldType(FieldTypeDTO.getType(type));
        field.setRealType(RealFieldTypeDTO.getType(realType));
        field.setImportance(importance);
        field.setExtremum(ExtremumTypeDTO.getType(extremum));
        setId(field);
        setRequired(field);
        setActive(field);
        if (field.getFieldType() == FieldTypeDTO.CHECK_BOX || field.getFieldType() == FieldTypeDTO.COMBO_BOX
                || field.getFieldType() == FieldTypeDTO.RADIO_BUTTON || field.getFieldType() == FieldTypeDTO.SLIDER) {
            setValues(field);
        } else {
            field.setValuesList(null);
        }
        JsonNode node = Json.parse(requestParameters.get(Constant.RESPONSE_COLLECTION));
        field.setProfilesCollectionDTO(Json.fromJson(node, ProfilesCollectionDTO.class));
        return field;
    }

    private void setId(FieldDTO field) {
        if (requestParameters.get(Constant.ID) != null && !Constant.EMPTY_STRING.equals(requestParameters.get(Constant.ID))) {
            field.setId(Integer.parseInt(requestParameters.get(Constant.ID)));
        }
    }

    private void setRequired(FieldDTO field) {
        field.setRequired(false);
        if (requestParameters.get(Constant.REQUIRED) != null) {
            field.setRequired(true);
        }
    }

    private void setActive(FieldDTO field) {
        field.setActive(false);
        if (requestParameters.get(Constant.ACTIVE) != null) {
            field.setActive(true);
        }
    }

    private void setValues(FieldDTO field) {
        List<ValueDTO> values = new ArrayList<>();
        String textarea = requestParameters.get(Constant.TEXT);
        String minValue = requestParameters.get(Constant.MIN);
        String maxValue = requestParameters.get(Constant.MAX);
        if (field.getFieldType() == FieldTypeDTO.SLIDER) {
            ValueDTO min = new ValueDTO(minValue);
            ValueDTO max = new ValueDTO(maxValue);
            values.add(min);
            values.add(max);
        } else {
            String[] valuesMass = textarea.split("\\s+");
            for (String str : valuesMass) {
                ValueDTO value = new ValueDTO(str);
                values.add(value);
            }
        }
        field.setValuesList(values);
    }
}
