package builder;

import static utils.Constant.EMPTY_VALUE;
import static utils.Constant.SPACE;
import static utils.Constant.EMPTY_STRING;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import models.dto.ProfileDTO;
import models.dto.ProfilesCollectionDTO;
import models.entities.Field;
import models.entities.FieldType;

/**
 * This class allows to get the profileDTO instance from http request
 */
public class ProfileBuilder {

    private Map<String, String[]> request;

    private List<Field> fields;

    private ProfilesCollectionDTO profilesCollectionDTO;

    public ProfileBuilder(Map<String, String[]> request, List<Field> fields, ProfilesCollectionDTO profilesCollectionDTO) {
        this.request = request;
        this.fields = fields;
        this.profilesCollectionDTO = profilesCollectionDTO;
    }

    public ProfileDTO getProfileDTOFromRequest() {
        List<String> requestParameters = fields.stream().map(this::getRequestParameter).collect(Collectors.toList());
        ProfileDTO profile = new ProfileDTO();
        profile.setInputParametersList(requestParameters);
        profile.setProfilesCollectionDTO(profilesCollectionDTO);
        return profile;
    }

    private String getRequestParameter(Field field) {
        if (request == null || !field.isActive() || request.get(field.getLabel()) == null) {
            return EMPTY_VALUE;
        } else {
            return getTypeField(field);
        }
    }

    private String getTypeField(Field field) {
        if (field.getFieldType() == FieldType.CHECK_BOX) {
            return getCheckboxValue(field);
        } else {
            return !EMPTY_STRING.equals(request.get(field.getLabel())[0]) ? request.get(field.getLabel())[0] : EMPTY_VALUE;
        }
    }

    private String getCheckboxValue(Field field) {
        String requestParameter;
        StringBuilder res = new StringBuilder();
        String[] checkboxValues = request.get(field.getLabel());
        for (String checboxValue : checkboxValues) {
            res.append(checboxValue).append(SPACE);
        }
        requestParameter = res.toString().trim();
        return requestParameter;
    }
}
