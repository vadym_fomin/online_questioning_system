package builder;

import java.util.List;

import models.dto.ValueDTO;

/**
 * This class  represents the values of the field with the type tekstarea
 * as a string split by '\n'
 */
public class TextareaContentBuilder {

    public static String getContent(List<ValueDTO> values) {
        StringBuilder res = new StringBuilder();
        for (ValueDTO value : values) {
            res.append(value.getFieldValue()).append("\n");
        }
        return res.toString();
    }
}
