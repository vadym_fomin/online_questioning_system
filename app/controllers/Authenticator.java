package controllers;

import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;
import utils.Constant;
import views.html.*;

/**
 * This class implements the role separation logic on the site
 */
public class Authenticator extends Security.Authenticator {

    @Override
    public String getUsername(Context context) {
        return context.session().get(Constant.SESSION_PARAMETER);
    }

    @Override
    public Result onUnauthorized(Context context) {
        return badRequest(creatingProfilesCollection.render(false)).as("text/html");
    }
}
