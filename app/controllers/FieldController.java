package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import builder.FieldBuilder;
import models.dto.FieldDTO;
import models.entities.Admin;
import models.entities.Field;
import models.entities.ProfilesCollection;
import play.Logger;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.*;
import utils.Constant;
import utils.Converter;
import views.html.*;

public class FieldController extends Controller {

    @Inject
    private ProfilesCollectionService profilesCollectionService;

    @Inject
    private FieldService fieldService;

    @Inject
    private FormFactory formFactory;

    @Inject
    private AdminService adminService;

    @Inject
    private ProfileService profileService;

    @Inject
    private FieldFormHandler fieldFormHandler;


    @Security.Authenticated(Authenticator.class)
    @Transactional
    public Result getAllFields() {
        Admin admin = Json.fromJson(Json.parse(session().get(Constant.SESSION_PARAMETER)), Admin.class);
        ProfilesCollection profilesCollection = profilesCollectionService.getProfilesCollectionByAdmin(admin);
        Collection fieldsDTO = Converter.getFieldsDTO(fieldService.getFieldsByProfilesCollection(profilesCollection));
        long profilesCount = profileService.getProfilesCount(profilesCollection);
        return ok(allFields.render(fieldsDTO, profilesCount, admin.getId()));
    }

    @Security.Authenticated(Authenticator.class)
    @Transactional
    public Result deleteField() {
        DynamicForm form = formFactory.form().bindFromRequest();
        int fieldId = Integer.parseInt(form.get(Constant.ID));
        fieldService.delete(fieldId);
        return ok("deleting is ok");
    }

    @Security.Authenticated(Authenticator.class)
    @Transactional
    public Result getFieldInformation(int id) {
        FieldDTO fieldDTO = null;
        Admin admin = Json.fromJson(Json.parse(session().get(Constant.SESSION_PARAMETER)), Admin.class);
        ProfilesCollection profilesCollection = profilesCollectionService.getProfilesCollectionByAdmin(admin);
        if (id != 0) {
            fieldDTO = Converter.getFieldDTO(fieldService.getById(id));
        }
        long profilesCount = profileService.getProfilesCount(profilesCollection);
        return ok(creatingField.render(fieldDTO, profilesCount, admin.getId()));
    }

    @Security.Authenticated(Authenticator.class)
    @Transactional
    public Result addField() {
        DynamicForm form = formFactory.form().bindFromRequest();
        Admin admin = Json.fromJson(Json.parse(session().get(Constant.SESSION_PARAMETER)), Admin.class);
        FieldDTO fieldDTO = fieldFormHandler.handle(form, admin);
        boolean isAdded = fieldService.add(fieldDTO);
        if (!isAdded) {
            return badRequest();
        }
        return ok("adding is ok");
    }

    @Security.Authenticated(Authenticator.class)
    @Transactional
    public Result editField() {
        DynamicForm form = formFactory.form().bindFromRequest();
        Admin admin = Json.fromJson(Json.parse(session().get(Constant.SESSION_PARAMETER)), Admin.class);
        FieldDTO fieldDTO = fieldFormHandler.handle(form, admin);
        boolean isUpdated = fieldService.edit(fieldDTO);
        if (!isUpdated) {
            return badRequest();
        }
        return ok("editing is ok");
    }

    @Transactional
    public Result getUserForm(int id) {
        Collection fieldsDTO = new ArrayList();
        Admin admin = adminService.getAdminById(id);
        if (admin != null) {
            ProfilesCollection profilesCollection = profilesCollectionService.getProfilesCollectionByAdmin(admin);
            List<Field> fields = new ArrayList<>(fieldService.getFieldsByProfilesCollection(profilesCollection));
            fieldsDTO = Converter.getFieldsDTO(fields);
        }
        return ok(userForm.render(fieldsDTO, admin.getId()));
    }
}
