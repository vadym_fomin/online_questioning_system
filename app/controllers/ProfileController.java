package controllers;


import com.fasterxml.jackson.databind.JsonNode;
import models.dto.ProfileDTO;
import models.entities.Admin;
import models.entities.Field;
import models.entities.Profile;
import models.entities.ProfilesCollection;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.*;
import utils.Constant;
import utils.Converter;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import views.html.*;

public class ProfileController extends Controller {

    @Inject
    private ProfileService profileService;

    @Inject
    private FieldService fieldService;

    @Inject
    private ProfilesCollectionService profilesCollectionService;

    @Inject
    private AdminService adminService;

    @Inject
    private ProfileFormHandler profileFormHandler;

    @Inject
    private OrderingService orderingService;

    @Security.Authenticated(Authenticator.class)
    @Transactional
    public  Result getOderedProfilesCollection() {
        Admin admin = Json.fromJson(Json.parse(session().get(Constant.SESSION_PARAMETER)), Admin.class);
        ProfilesCollection profilesCollection = profilesCollectionService.getProfilesCollectionByAdmin(admin);
        Collection<Profile> profiles = orderingService.getOrderingProfiles(profilesCollection);
        return ok(Json.toJson(profiles));
    }


    @Transactional
    public Result addProfile(int adminId) {
        Map<String, String[]> request = Controller.request().body().asFormUrlEncoded();
        Admin admin = adminService.getAdminById(adminId);
        ProfileDTO profileDTO = profileFormHandler.handle(request, admin);
        boolean isAdded = profileService.add(profileDTO);
        if (!isAdded) {
            return badRequest();
        }
        return ok();
    }

    @Security.Authenticated(Authenticator.class)
    @Transactional
    public Result getProfilesCollection() {
        Admin admin = Json.fromJson(Json.parse(session().get(Constant.SESSION_PARAMETER)), Admin.class);
        ProfilesCollection profilesCollection = profilesCollectionService.getProfilesCollectionByAdmin(admin);
        Set<Field> fields = fieldService.getFieldsByProfilesCollection(profilesCollection);
        Set<Profile> profiles = profileService.getProfilesByProfilesCollection(profilesCollection);
        long profilesCount = profileService.getProfilesCount(profilesCollection);
        return ok(allProfiles.render(Converter.getFieldsDTO(fields), Converter.getProfilesDTO(profiles), profilesCount, admin.getId()));

    }
}

