package controllers;

import javax.inject.Inject;

import com.fasterxml.jackson.databind.JsonNode;
import models.dto.AdminDTO;
import models.entities.Admin;
import models.entities.ProfilesCollection;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.AdminService;
import services.PDFService;
import services.ProfilesCollectionService;
import utils.Constant;
import utils.Converter;
import views.html.*;

import java.io.File;

/**
 * This controller includes an action method handles requests for authentication
 */
public class UserController extends Controller {
    @Inject
    private FormFactory formFactory;

    @Inject
    private AdminService adminService;

    @Inject
    private ProfilesCollectionService profilesCollectionService;

    @Inject
    private PDFService pdfService;

    @Transactional
    public Result login() {
        Form<AdminDTO> form = formFactory.form(AdminDTO.class).bindFromRequest();
        AdminDTO adminDTO = adminService.createFromForm(form);
        if (adminDTO == null) {
            return badRequest();
        }
        JsonNode adminNode = Json.toJson(adminDTO);
        Controller.session().put(Constant.SESSION_PARAMETER, adminNode.toString());
        return redirect(Constant.ALL_FILEDS_PAGE);
    }

    public Result logout() {
        session().remove(Constant.SESSION_PARAMETER);
        return redirect(Constant.MAIN_PAGE);
    }

    @Transactional
    public Result createProfilesCollection() {
        Form<AdminDTO> form = formFactory.form(AdminDTO.class).bindFromRequest();
        AdminDTO adminDTO = profilesCollectionService.createFromForm(form);
        if (adminDTO == null) {
            return badRequest();
        }
        JsonNode adminNode = Json.toJson(adminDTO);
        Controller.session().put(Constant.SESSION_PARAMETER, adminNode.toString());
        return redirect(Constant.ALL_FILEDS_PAGE);
    }

    public Result getMainPage() {
        String sessionContent = session().get(Constant.SESSION_PARAMETER);
        boolean isAdmin = false;
        if (sessionContent != null) {
            isAdmin = true;
        }
        return ok(creatingProfilesCollection.render(isAdmin));
    }

    @Security.Authenticated(Authenticator.class)
    @Transactional
    public Result getFile() {
        response().setContentType(Constant.CONTENT_TYPE);
        Admin admin = Json.fromJson(Json.parse(session().get(Constant.SESSION_PARAMETER)), Admin.class);
        ProfilesCollection profilesCollection = profilesCollectionService.getProfilesCollectionByAdmin(admin);
        pdfService.createPDF(profilesCollection);
        return ok(new File(Constant.PDF_FILE));
    }

}