package dao;

import com.google.inject.ImplementedBy;

import dao.impl.AdminDaoImpl;
import models.entities.Admin;

/**
 * This interface declares the basic methods for working with Admin entity
 */
@ImplementedBy(AdminDaoImpl.class)
public interface AdminDao extends BasicDao<Admin> {

    boolean isExist(Admin admin);

    Admin getAdminByName(String name);

    boolean registerAdmin(Admin admin);
}
