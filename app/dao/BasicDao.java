package dao;

import java.util.Set;

/**
 * This interface declares the basic methods for working with entities DAO
 */
public interface BasicDao<T> {

    void insert(T entity);

    T get(int id);

    Set<T> getAll();

    void delete(T entity);
}
