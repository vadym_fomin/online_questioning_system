package dao;

import java.util.Set;

import com.google.inject.ImplementedBy;
import dao.impl.FieldDaoImpl;
import models.entities.Field;
import models.entities.ProfilesCollection;

@ImplementedBy(FieldDaoImpl.class)
public interface FieldDao extends BasicDao<Field> {

    void update(Field field);

    void delete(Field field);

    Set<Field> getFieldsByProfilesCollection(ProfilesCollection profilesCollection);
}
