package dao;

import com.google.inject.ImplementedBy;
import dao.impl.ProfileDaoImpl;
import models.entities.Profile;
import models.entities.ProfilesCollection;

import java.util.Set;

@ImplementedBy(ProfileDaoImpl.class)
public interface ProfileDao extends BasicDao<Profile> {

    long getProfilesCountInProfilesCollection(ProfilesCollection profilesCollection);

    Set<Profile> getProfilesByProfilesCollection(ProfilesCollection profilesCollection);
}
