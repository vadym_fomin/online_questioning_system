package dao;

import com.google.inject.ImplementedBy;

import dao.impl.ProfilesCollectionDaoImpl;
import models.entities.Admin;
import models.entities.ProfilesCollection;

@ImplementedBy(ProfilesCollectionDaoImpl.class)
public interface ProfilesCollectionDao extends BasicDao<ProfilesCollection> {

    ProfilesCollection getProfilesCollectionByAdmin(Admin admin);
}
