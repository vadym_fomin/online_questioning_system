package dao.impl;

import java.util.LinkedHashSet;
import java.util.Set;

import com.mysema.query.jpa.impl.JPAQuery;

import dao.AdminDao;
import models.entities.Admin;
import models.querytemplates.AdminQueryTemplates;
import play.db.jpa.JPA;
import javax.persistence.EntityManager;

public class AdminDaoImpl implements AdminDao {

    @Override
    public boolean isExist(Admin admin) {
        AdminQueryTemplates qAdmin = AdminQueryTemplates.admin;
        JPAQuery query = new JPAQuery(JPA.em());
        Admin foundAdmin = query.from(qAdmin)
                .where(qAdmin.name.eq(admin.getName())
                .and(qAdmin.password.eq(admin.getPassword())))
                .uniqueResult(qAdmin);
        //TODO change to foundAdmin != null or admin.equals(foundAdmin) (change Admin.class equals)
        return !(foundAdmin == null);
    }

    @Override
    public Admin getAdminByName(String name) {
        AdminQueryTemplates qAdmin = AdminQueryTemplates.admin;
        JPAQuery query = new JPAQuery(JPA.em());
        return query.from(qAdmin).where(qAdmin.name.eq(name)).uniqueResult(qAdmin);
    }

    @Override
    public boolean registerAdmin(Admin admin) {
        boolean isRegistered = false;
        EntityManager em = JPA.em();
        AdminQueryTemplates qAdmin = AdminQueryTemplates.admin;
        JPAQuery query = new JPAQuery(em);
        Admin foundedAdmin = query.from(qAdmin).where(qAdmin.name.eq(admin.getName())).uniqueResult(qAdmin);
        if (foundedAdmin == null) {
            em.persist(admin);
            isRegistered = true;
        }
        return isRegistered;
    }

    @Override
    public void insert(Admin admin) {
        JPA.em().persist(admin);
    }

    @Override
    public Admin get(int id) {
        return JPA.em().find(Admin.class, id);
    }

    @Override
    public Set<Admin> getAll() {
        AdminQueryTemplates qAdmin = AdminQueryTemplates.admin;
        JPAQuery query = new JPAQuery(JPA.em());
        return new LinkedHashSet<>(query.from(qAdmin).list(qAdmin));
    }

    @Override
    public void delete(Admin entity) {
        JPA.em().remove(entity);
    }
}
