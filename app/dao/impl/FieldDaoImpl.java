package dao.impl;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.mysema.query.jpa.impl.JPAQuery;
import dao.FieldDao;
import dao.ProfileDao;
import models.entities.Field;
import models.entities.Profile;
import models.entities.ProfilesCollection;
import models.querytemplates.FieldQueryTemplates;
import play.Logger;
import play.db.jpa.JPA;
import utils.Constant;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class FieldDaoImpl implements FieldDao {

    private ProfileDao profileDao;

    @Inject
    public FieldDaoImpl(ProfileDao profileDao) {
        this.profileDao = profileDao;
    }


    @Override
    public void update(Field field) {
        JPA.em().merge(field);
        Logger.debug("Field '" + field.getLabel() + "' was updated");
    }

    @Override
    public void delete(Field field) {
        EntityManager manager = JPA.em();
        int indexNumber = getFieldIndexNumber(field);
        Set<Profile> profiles = profileDao.getAll();
        manager.remove(manager.contains(field) ? field : manager.merge(field));
        if (profiles != null) {
            for (Profile profile : profiles) {
                List<String> inputParametersList = profile.getInputParametersList();
                inputParametersList.remove(indexNumber);
                profile.setInputParametersList(inputParametersList);
                manager.merge(profile);
            }
        }
        Logger.debug("Field '" + field.getLabel() + "' was deleted");
    }

    @Override
    public Set<Field> getFieldsByProfilesCollection(ProfilesCollection profilesCollection) {
        EntityManager manager = JPA.em();
        FieldQueryTemplates qField = FieldQueryTemplates.field;
        JPAQuery query = new JPAQuery(manager);
        return new TreeSet<>(query.from(qField)
                .where(qField.profilesCollection.id.eq(profilesCollection.getId())).list(qField));
    }

    @Override
    public void insert(Field entity) {
        EntityManager manager = JPA.em();
        Set<Profile> profiles = profileDao.getAll();
        if (profiles != null) {
            for (Profile profile : profiles) {
                List<String> inputParametersList = profile.getInputParametersList();
                inputParametersList.add(Constant.EMPTY_VALUE);
                profile.setInputParametersList(inputParametersList);
                manager.merge(profile);
            }
        }
        manager.merge(entity);
        Logger.debug("Field '" + entity.getLabel() + "' was created");
    }

    @Override
    public Field get(int id) {
        return JPA.em().find(Field.class, id);
    }

    @Override
    public Set<Field> getAll() {
        EntityManager manager = JPA.em();
        FieldQueryTemplates qField = FieldQueryTemplates.field;
        JPAQuery query = new JPAQuery(manager);
        Set<Field> fields = new TreeSet<>(query.from(qField).list(qField));
        return fields;
    }

    /**
     * @return the ordinal number of the field in a fields set
     */
    private int getFieldIndexNumber(Field removableField) {
        int indexNumber = -1;
        Set<Field> fields = getAll();
        for (Field field : fields) {
            indexNumber++;
            if (field.getId() == removableField.getId()) {
                break;
            }
        }
        return indexNumber;
    }
}
