package dao.impl;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;

import com.mysema.query.jpa.impl.JPAQuery;

import dao.ProfileDao;
import models.entities.Profile;
import models.entities.ProfilesCollection;
import models.querytemplates.ProfileQueryTemplates;
import play.Logger;
import play.db.jpa.JPA;

public class ProfileDaoImpl implements ProfileDao {

    private static final String GET_PROFILES_BY_PROFILES_COLLECTION = "from Profile p join p.profilesCollection pc where pc.id=:id ";

    @Override
    @SuppressWarnings("ConstantConditions")
    public long getProfilesCountInProfilesCollection(ProfilesCollection profilesCollection) {
        EntityManager manager = JPA.em();
        ProfileQueryTemplates qProfile = ProfileQueryTemplates.profile;
        JPAQuery query = new JPAQuery(manager);
        return query.from(qProfile).where(qProfile.profilesCollection.id.eq(profilesCollection.getId())).uniqueResult(qProfile.count());
    }

    @Override
    public Set<Profile> getProfilesByProfilesCollection(ProfilesCollection profilesCollection) {
        EntityManager manager = JPA.em();
        ProfileQueryTemplates qProfile = ProfileQueryTemplates.profile;
        JPAQuery query = new JPAQuery(manager);
        return new LinkedHashSet<>(query.from(qProfile).where(qProfile.profilesCollection.id.eq(profilesCollection.getId())).list(qProfile));
    }

    @Override
    public void insert(Profile entity) {
        JPA.em().merge(entity);
        Logger.debug("Profile was created");
    }

    @Override
    public Profile get(int id) {
        return JPA.em().find(Profile.class, id);
    }

    @Override
    public Set<Profile> getAll() {
        EntityManager manager = JPA.em();
        ProfileQueryTemplates qProfile = ProfileQueryTemplates.profile;
        JPAQuery query = new JPAQuery(manager);
        Set<Profile> profiles = new LinkedHashSet<>(query.from(qProfile).list(qProfile));
        return profiles;
    }

    @Override
    public void delete(Profile entity) {
        JPA.em().remove(entity);
    }
}
