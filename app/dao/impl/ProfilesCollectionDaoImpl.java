package dao.impl;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;

import com.mysema.query.jpa.impl.JPAQuery;

import dao.ProfilesCollectionDao;
import models.entities.Admin;
import models.entities.ProfilesCollection;
import models.querytemplates.ProfilesCollectionQueryTemplates;
import play.db.jpa.JPA;

public class ProfilesCollectionDaoImpl implements ProfilesCollectionDao {

    @Override
    public ProfilesCollection getProfilesCollectionByAdmin(Admin admin) {
        EntityManager manager = JPA.em();
        ProfilesCollectionQueryTemplates qProfilesCollection = ProfilesCollectionQueryTemplates.profilesCollection;
        JPAQuery query = new JPAQuery(manager);
        ProfilesCollection profilesCollection = query.from(qProfilesCollection)
                .where(qProfilesCollection.admin.name.eq(admin.getName()))
                .uniqueResult(qProfilesCollection);
        return profilesCollection != null ? profilesCollection : new ProfilesCollection();
    }

    @Override
    public void insert(ProfilesCollection entity) {
        JPA.em().persist(entity);
    }

    @Override
    public ProfilesCollection get(int id) {
        return JPA.em().find(ProfilesCollection.class, id);
    }

    @Override
    public Set<ProfilesCollection> getAll() {
        ProfilesCollectionQueryTemplates qProfilesCollection = ProfilesCollectionQueryTemplates.profilesCollection;
        JPAQuery query = new JPAQuery(JPA.em());
        return new LinkedHashSet<>(query.from(qProfilesCollection).list(qProfilesCollection));
    }

    @Override
    public void delete(ProfilesCollection entity) {
        JPA.em().remove(entity);
    }
}
