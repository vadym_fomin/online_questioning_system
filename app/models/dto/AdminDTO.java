package models.dto;

import play.data.validation.Constraints.Pattern;
import play.data.validation.Constraints.Required;

public class AdminDTO {
    
    private Integer id;

    @Required
    @Pattern(value = "[a-zA-Z]{5,15}")
    private String name;

    @Required
    @Pattern(value = "[a-zA-Z][a-zA-Z0-9-_.]{5,19}")
    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
