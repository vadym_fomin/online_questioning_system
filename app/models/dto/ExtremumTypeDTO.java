package models.dto;



/**
 * Created by Vadim on 11.06.2017.
 */
public enum ExtremumTypeDTO {

    MIN("min"), MAX("max");

    private String name;

    ExtremumTypeDTO (String name) {
        this.name = name;
    }

    public static ExtremumTypeDTO getType(String name) {
        ExtremumTypeDTO [] types = ExtremumTypeDTO .values();
        for (ExtremumTypeDTO  type : types) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
