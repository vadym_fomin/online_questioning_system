package models.dto;

import java.util.List;

public class FieldDTO {

    private Integer id;

    private String label;

    private FieldTypeDTO fieldType;

    private List<ValueDTO> valuesList;

    private RealFieldTypeDTO realType;

    private Double importance;

    public RealFieldTypeDTO getRealType() {
        return realType;
    }

    public void setRealType(RealFieldTypeDTO realType) {
        this.realType = realType;
    }

    public Double getImportance() {
        return importance;
    }

    public void setImportance(Double importance) {
        this.importance = importance;
    }

    public ExtremumTypeDTO getExtremum() {
        return extremum;
    }

    public void setExtremum(ExtremumTypeDTO extremum) {
        this.extremum = extremum;
    }

    private ExtremumTypeDTO extremum;

    private boolean required;

    private boolean isActive;

    private ProfilesCollectionDTO profilesCollectionDTO;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public FieldTypeDTO getFieldType() {
        return fieldType;
    }

    public void setFieldType(FieldTypeDTO fieldType) {
        this.fieldType = fieldType;
    }

    public List<ValueDTO> getValuesList() {
        return valuesList;
    }

    public void setValuesList(List<ValueDTO> valuesList) {
        this.valuesList = valuesList;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public ProfilesCollectionDTO getProfilesCollectionDTO() {
        return profilesCollectionDTO;
    }

    public void setProfilesCollectionDTO(ProfilesCollectionDTO profilesCollectionDTO) {
        this.profilesCollectionDTO = profilesCollectionDTO;
    }
}
