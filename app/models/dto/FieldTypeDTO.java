package models.dto;

public enum FieldTypeDTO {
    SINGLE_LINE_TEXT("single line text"), MULTI_LINE_TEXT("multi line text"), RADIO_BUTTON("radio button"), CHECK_BOX("check box"), COMBO_BOX(
            "combo box"), DATE("date"), SLIDER("slider");

    private String name;

    FieldTypeDTO(String name) {
        this.name = name;
    }

    public static FieldTypeDTO getType(String name) {
        FieldTypeDTO[] types = FieldTypeDTO.values();
        for (FieldTypeDTO type : types) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;

    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
