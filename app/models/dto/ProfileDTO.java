package models.dto;

import java.util.List;

public class ProfileDTO {

    private List<String> inputParametersList;

    private ProfilesCollectionDTO profilesCollectionDTO;

    public List<String> getInputParametersList() {
        return inputParametersList;
    }

    public void setInputParametersList(List<String> inputParametersList) {
        this.inputParametersList = inputParametersList;
    }

    public ProfilesCollectionDTO getProfilesCollectionDTO() {
        return profilesCollectionDTO;
    }

    public void setProfilesCollectionDTO(ProfilesCollectionDTO profilesCollectionDTO) {
        this.profilesCollectionDTO = profilesCollectionDTO;
    }
}
