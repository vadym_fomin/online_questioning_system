package models.dto;

/**
 * This class encapsulates a list of fields and profiles to display them via scala template
 */

public class ProfilesCollectionDTO {

    private Integer id;

    private AdminDTO admin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AdminDTO getAdmin() {
        return admin;
    }

    public void setAdmin(AdminDTO admin) {
        this.admin = admin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        ProfilesCollectionDTO that = (ProfilesCollectionDTO) o;

        return admin.equals(that.admin);

    }

    @Override
    public int hashCode() {
        return admin.hashCode();
    }
}
