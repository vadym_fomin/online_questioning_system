package models.dto;

/**
 * Created by Vadim on 11.06.2017.
 */
public enum RealFieldTypeDTO {

    STRING("string"), NUMBER("number");

    private String name;

    RealFieldTypeDTO(String name) {
        this.name = name;
    }

    public static RealFieldTypeDTO getType(String name) {
        RealFieldTypeDTO[] types = RealFieldTypeDTO.values();
        for (RealFieldTypeDTO type : types) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
