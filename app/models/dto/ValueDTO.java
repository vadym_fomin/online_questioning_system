package models.dto;

public class ValueDTO {

    private String fieldValue;

    public ValueDTO() {
    }

    public ValueDTO(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

}
