package models.entities;

/**
 * Created by Vadim on 11.06.2017.
 */
public enum ExtremumType {

    MIN("min"), MAX("max");

    private String name;

    ExtremumType (String name) {
        this.name = name;
    }

    public static ExtremumType  getType(String name) {
        ExtremumType [] types = ExtremumType .values();
        for (ExtremumType  type : types) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
