package models.entities;

import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

/**
 * This class is a mapping on the field table in the database
 */

@Entity
@Table(name = "field")
public class Field implements Comparable<Field> {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;

    @Column(name = "name")
    private String label;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private FieldType fieldType;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Value> valuesList;

    @Enumerated(EnumType.STRING)
    @Column(name = "realType")
    private RealFieldType realType;

    @Column(name = "importance")
    private Double importance;

    @Enumerated(EnumType.STRING)
    @Column(name = "extremum")
    private ExtremumType extremum;

    public  RealFieldType getRealType() {
        return realType;
    }

    public void setRealType(RealFieldType realType) {
        this.realType = realType;
    }

    public Double getImportance() {
        return importance;
    }

    public void setImportance(Double importance) {
        this.importance = importance;
    }

    public ExtremumType getExtremum() {
        return extremum;
    }

    public void setExtremum(ExtremumType extremum) {
        this.extremum = extremum;
    }

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private ProfilesCollection profilesCollection;

    private boolean required;

    private boolean isActive;

    public Field() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(FieldType fieldType) {
        this.fieldType = fieldType;
    }

    public List<Value> getValuesList() {
        return valuesList;
    }

    public void setValuesList(List<Value> valuesList) {
        this.valuesList = valuesList;
    }

    public ProfilesCollection getProfilesCollection() {
        return profilesCollection;
    }

    public void setProfilesCollection(ProfilesCollection profilesCollection) {
        this.profilesCollection = profilesCollection;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public int compareTo(Field field) {
        return id - field.getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Field field = (Field) o;

        if (required != field.required)
            return false;
        if (isActive != field.isActive)
            return false;
        if (id != null ? !id.equals(field.id) : field.id != null)
            return false;
        if (label != null ? !label.equals(field.label) : field.label != null)
            return false;
        if (fieldType != field.fieldType)
            return false;
        if (valuesList != null ? !valuesList.equals(field.valuesList) : field.valuesList != null)
            return false;
        return profilesCollection != null ? profilesCollection.equals(field.profilesCollection) : field.profilesCollection == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (label != null ? label.hashCode() : 0);
        result = 31 * result + (fieldType != null ? fieldType.hashCode() : 0);
        result = 31 * result + (valuesList != null ? valuesList.hashCode() : 0);
        result = 31 * result + (profilesCollection != null ? profilesCollection.hashCode() : 0);
        result = 31 * result + (required ? 1 : 0);
        result = 31 * result + (isActive ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Field{" + "id=" + id + ", label='" + label + '\'' + ", fieldType=" + fieldType + ", valuesList=" + valuesList
                + ", profilesCollection=" + profilesCollection + ", required=" + required + ", isActive=" + isActive + '}';
    }
}
