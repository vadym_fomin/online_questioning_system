package models.entities;

/**
 * This enum represents all possible types of data fields
 */

public enum FieldType {

    SINGLE_LINE_TEXT("single line text"), MULTI_LINE_TEXT("multi line text"), RADIO_BUTTON("radio button"),
        CHECK_BOX("check box"), COMBO_BOX("combo box"), DATE("date"), SLIDER("slider");

    private String name;

    FieldType(String name) {
        this.name = name;
    }

    public static FieldType getType(String name) {
        FieldType[] types = FieldType.values();
        for (FieldType type : types) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
