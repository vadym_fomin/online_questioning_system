package models.entities;

import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

/**
 * This class is a mapping on the profile(response) table in the database. It stores the data entered by the user in the form of a list of strings
 */

@Entity
@Table(name = "profile")
public class Profile {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer profileId;

    @ElementCollection(fetch = FetchType.LAZY)
    private List<String> inputParametersList;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private ProfilesCollection profilesCollection;

    public Profile() {
    }

    public ProfilesCollection getProfilesCollection() {
        return profilesCollection;
    }

    public void setProfilesCollection(ProfilesCollection profilesCollection) {
        this.profilesCollection = profilesCollection;
    }

    public Integer getProfileId() {
        return profileId;
    }

    public void setProfileId(Integer profileId) {
        this.profileId = profileId;
    }

    public List<String> getInputParametersList() {
        return inputParametersList;
    }

    public void setInputParametersList(List<String> inputParametersList) {
        this.inputParametersList = inputParametersList;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((profileId == null) ? 0 : profileId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Profile other = (Profile) obj;
        if (profileId == null) {
            if (other.profileId != null)
                return false;
        } else if (!profileId.equals(other.profileId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Profile{" + "profileId=" + profileId + ", inputParametersList=" + inputParametersList + ", profilesCollection=" + profilesCollection
                + '}';
    }
}
