package models.entities;

/**
 * Created by Vadim on 11.06.2017.
 */
public enum RealFieldType {

    STRING("string"), NUMBER("number");

    private String name;

    RealFieldType(String name) {
        this.name = name;
    }

    public static RealFieldType getType(String name) {
        RealFieldType[] types = RealFieldType.values();
        for (RealFieldType type : types) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
