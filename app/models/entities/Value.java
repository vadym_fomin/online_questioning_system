package models.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * This class represents the possible value taken by the fields
 */

@Entity
@Table(name = "value")
public class Value {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer valueId;

    private String fieldValue;

    public Value() {
    }

    public Value(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public Integer getValueId() {
        return valueId;
    }

    public void setValueId(Integer valueId) {
        this.valueId = valueId;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Value value = (Value) o;

        if (valueId != null ? !valueId.equals(value.valueId) : value.valueId != null)
            return false;
        return fieldValue != null ? fieldValue.equals(value.fieldValue) : value.fieldValue == null;

    }

    @Override
    public int hashCode() {
        int result = valueId != null ? valueId.hashCode() : 0;
        result = 31 * result + (fieldValue != null ? fieldValue.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Value{" + "valueId=" + valueId + ", fieldValue='" + fieldValue + '\'' + '}';
    }
}
