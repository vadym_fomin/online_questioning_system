package models.idealPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vadim on 11.06.2017.
 */
public class IdealPoint {

    private List<Number> cords = new ArrayList<>();

    private List<Double> cordsImportance;


    public List<Double> getCordsImportance() {
        return cordsImportance;
    }

    public void setCordsImportance(List<Double> cordsImportance) {
        this.cordsImportance = cordsImportance;
    }

    public List<Number> getCords() {
        return cords;
    }

    public void setCords(List<Number> cords) {
        this.cords = cords;
    }

    @Override
    public String toString() {
        return "IdealPoint{" +
                "cords=" + cords +
                ", cordsImportance=" + cordsImportance +
                '}';
    }
}
