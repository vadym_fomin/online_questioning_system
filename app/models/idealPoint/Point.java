package models.idealPoint;


import play.Logger;

import java.util.ArrayList;
import java.util.List;

public class Point implements Comparable<Point> {

    private List<Number> cords = new ArrayList<>();

    private static IdealPoint idealPoint;

    private static List<Number> maxValues;

    private static List<Number> minValues;

    public static List<Number> getMaxValue() {
        return maxValues;
    }

    public static void setMaxValue(List<Number> maxValues) {
        Point.maxValues = maxValues;
    }

    public List<Number> getMinValue() {
        return minValues;
    }

    public static void setMinValue(List<Number> minValues) {
        Point.minValues = minValues;
    }

    public List<Number> getCords() {
        return cords;
    }

    public void setCords(List<Number> cords) {
        this.cords = cords;
    }

    public static IdealPoint getIdealPoint() {
        return idealPoint;
    }

    public static void setIdealPoint(IdealPoint idealPoint) {
        Point.idealPoint = idealPoint;
    }

    public Double getDistance() {
        double distance = 0.0;
        Number cord = 0.0;
        for (int i = 0; i < idealPoint.getCords().size(); i++) {
            cord = (cords.get(i).doubleValue() - minValues.get(i).doubleValue()) / (maxValues.get(i).doubleValue() - minValues.get(i).doubleValue());
            distance += Math.pow(cord.doubleValue() - idealPoint.getCords().get(i).doubleValue(), 2) * idealPoint.getCordsImportance().get(i);
        }
        return Math.sqrt(distance);
    }

    @Override
    public int compareTo(Point point) {
        int result = getDistance().compareTo(point.getDistance());
        return result != 0 ? result : -1;
    }

    @Override
    public String toString() {
        return "Point{" +
                "cords=" + cords +
                '}';
    }
}
