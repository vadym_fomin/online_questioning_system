package models.querytemplates;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import models.entities.Admin;


/**
 * AdminQueryTemplates is a Querydsl query type for Admin
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class AdminQueryTemplates extends EntityPathBase<Admin> {

    public static final AdminQueryTemplates admin = new AdminQueryTemplates("admin");

    public final NumberPath<Integer> adminId = createNumber("adminId", Integer.class);

    public final StringPath name = createString("name");

    public final StringPath password = createString("password");

    public AdminQueryTemplates(String variable) {
        super(Admin.class, forVariable(variable));
    }

    public AdminQueryTemplates(Path<? extends Admin> path) {
        super(path.getType(), path.getMetadata());
    }

    public AdminQueryTemplates(PathMetadata<?> metadata) {
        super(Admin.class, metadata);
    }

}

