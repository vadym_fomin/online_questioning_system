package models.querytemplates;

import static com.mysema.query.types.PathMetadataFactory.forVariable;

import javax.annotation.Generated;

import com.mysema.query.types.Path;
import com.mysema.query.types.PathMetadata;
import com.mysema.query.types.path.*;

import models.entities.Field;
import models.entities.FieldType;
import models.entities.Value;

/**
 * FieldQueryTemplates is a Querydsl query type for Field
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class FieldQueryTemplates extends EntityPathBase<Field> {

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final FieldQueryTemplates field = new FieldQueryTemplates("field");

    public final EnumPath<FieldType> fieldType = createEnum("fieldType", FieldType.class);

    public final NumberPath<Integer> id = createNumber("fieldId", Integer.class);

    public final BooleanPath isActive = createBoolean("isActive");

    public final StringPath label = createString("label");

    public final ProfilesCollectionQueryTemplates profilesCollection;

    public final BooleanPath required = createBoolean("required");

    public final ListPath<Value, ValueQueryTemplates> valuesList = this.<Value, ValueQueryTemplates> createList("valuesList", Value.class,
            ValueQueryTemplates.class, PathInits.DIRECT2);

    public FieldQueryTemplates(String variable) {
        this(Field.class, forVariable(variable), INITS);
    }

    public FieldQueryTemplates(Path<? extends Field> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public FieldQueryTemplates(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public FieldQueryTemplates(PathMetadata<?> metadata, PathInits inits) {
        this(Field.class, metadata, inits);
    }

    public FieldQueryTemplates(Class<? extends Field> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.profilesCollection = inits.isInitialized("profilesCollection")
                ? new ProfilesCollectionQueryTemplates(forProperty("profilesCollection"), inits.get("profilesCollection")) : null;
    }

}
