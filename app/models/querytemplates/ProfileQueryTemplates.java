package models.querytemplates;

import static com.mysema.query.types.PathMetadataFactory.forVariable;

import javax.annotation.Generated;

import com.mysema.query.types.Path;
import com.mysema.query.types.PathMetadata;
import com.mysema.query.types.path.*;

import models.entities.Profile;

/**
 * ProfileQueryTemplates is a Querydsl query type for Profile
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class ProfileQueryTemplates extends EntityPathBase<Profile> {

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final ProfileQueryTemplates profile = new ProfileQueryTemplates("profile");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final ListPath<String, StringPath> inputParametersList = this.createList("inputParametersList", String.class, StringPath.class,
            PathInits.DIRECT2);

    public final ProfilesCollectionQueryTemplates profilesCollection;

    public ProfileQueryTemplates(String variable) {
        this(Profile.class, forVariable(variable), INITS);
    }

    public ProfileQueryTemplates(Path<? extends Profile> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public ProfileQueryTemplates(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public ProfileQueryTemplates(PathMetadata<?> metadata, PathInits inits) {
        this(Profile.class, metadata, inits);
    }

    public ProfileQueryTemplates(Class<? extends Profile> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.profilesCollection = inits.isInitialized("profilesCollection")
                ? new ProfilesCollectionQueryTemplates(forProperty("profilesCollection"), inits.get("profilesCollection")) : null;
    }
}
