package models.querytemplates;

import static com.mysema.query.types.PathMetadataFactory.forVariable;

import javax.annotation.Generated;

import com.mysema.query.types.Path;
import com.mysema.query.types.PathMetadata;
import com.mysema.query.types.path.EntityPathBase;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathInits;

import models.entities.ProfilesCollection;


/**
 * ProfilesCollectionQueryTemplates is a Querydsl query type for ProfilesCollection
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class ProfilesCollectionQueryTemplates extends EntityPathBase<ProfilesCollection> {

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final ProfilesCollectionQueryTemplates profilesCollection = new ProfilesCollectionQueryTemplates("profilesCollection");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final AdminQueryTemplates admin;

    public ProfilesCollectionQueryTemplates(String variable) {
        this(ProfilesCollection.class, forVariable(variable), INITS);
    }

    public ProfilesCollectionQueryTemplates(Path<? extends ProfilesCollection> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public ProfilesCollectionQueryTemplates(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public ProfilesCollectionQueryTemplates(PathMetadata<?> metadata, PathInits inits) {
        this(ProfilesCollection.class, metadata, inits);
    }

    public ProfilesCollectionQueryTemplates(Class<? extends ProfilesCollection> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.admin = inits.isInitialized("admin") ? new AdminQueryTemplates(forProperty("admin")) : null;
    }

}

