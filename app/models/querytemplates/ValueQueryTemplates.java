package models.querytemplates;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import models.entities.Value;


/**
 * ValueQueryTemplates is a Querydsl query type for Value
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class ValueQueryTemplates extends EntityPathBase<Value> {

    public static final ValueQueryTemplates value = new ValueQueryTemplates("value1");

    public final StringPath fieldValue = createString("fieldValue");

    public final NumberPath<Integer> id = createNumber("valueId", Integer.class);

    public ValueQueryTemplates(String variable) {
        super(Value.class, forVariable(variable));
    }

    public ValueQueryTemplates(Path<? extends Value> path) {
        super(path.getType(), path.getMetadata());
    }

    public ValueQueryTemplates(PathMetadata<?> metadata) {
        super(Value.class, metadata);
    }

}

