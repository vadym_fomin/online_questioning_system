package services;

import com.google.inject.ImplementedBy;

import models.dto.AdminDTO;
import models.entities.Admin;
import play.data.Form;
import services.impl.AdminServiceImpl;


@ImplementedBy(AdminServiceImpl.class)
public interface AdminService {

    boolean logIn(Admin admin);

    boolean create(Admin admin);

    Admin getAdminByName(String name);

    Admin getAdminById(int id);

    AdminDTO createFromForm(Form<AdminDTO> form);
}
