package services;

import com.google.inject.ImplementedBy;
import models.dto.FieldDTO;
import models.entities.Admin;
import play.data.DynamicForm;
import services.impl.FieldFormHandlerImpl;

/**
 * Created by Vadim on 13.12.2016.
 */
@ImplementedBy(FieldFormHandlerImpl.class)
public interface FieldFormHandler {

    FieldDTO handle(DynamicForm dynamicForm, Admin admin);
}
