package services;

import java.util.Set;

import com.google.inject.ImplementedBy;

import models.dto.FieldDTO;
import models.entities.Field;
import models.entities.ProfilesCollection;
import services.impl.FieldServiceImpl;


/**
 * This interface defines a methods for implementation by FieldServiceImpl
 */

@ImplementedBy(FieldServiceImpl.class)
public interface FieldService {

    void delete(int id);

    Set<Field> getFieldsByProfilesCollection(ProfilesCollection profilesCollection);

    boolean add(FieldDTO field);

    boolean edit(FieldDTO field);

    Field getById(int id);


}
