package services;


import com.google.inject.ImplementedBy;
import models.entities.ProfilesCollection;

@ImplementedBy(services.impl.PDFServiceImpl.class)
public interface PDFService {

    void createPDF(ProfilesCollection profilesCollection);

}
