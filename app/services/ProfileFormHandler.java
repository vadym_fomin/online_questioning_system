package services;

import com.google.inject.ImplementedBy;
import models.dto.ProfileDTO;
import models.entities.Admin;
import services.impl.ProfileFormHandlerImpl;

import java.util.Map;

/**
 * Created by Vadim on 13.12.2016.
 */
@ImplementedBy(ProfileFormHandlerImpl.class)
public interface ProfileFormHandler {

    ProfileDTO handle(Map<String, String[]> request, Admin admin);
}