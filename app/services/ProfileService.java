package services;

import com.google.inject.ImplementedBy;
import models.dto.ProfileDTO;
import models.entities.Profile;
import models.entities.ProfilesCollection;
import services.impl.ProfileServiceImpl;

import java.util.Set;

@ImplementedBy(ProfileServiceImpl.class)
public interface ProfileService {

    boolean add(ProfileDTO profileDTO);

    Set<Profile> getProfilesByProfilesCollection(ProfilesCollection profilesCollection);

    long getProfilesCount(ProfilesCollection profilesCollection);
}
