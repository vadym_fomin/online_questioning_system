package services;

import com.google.inject.ImplementedBy;
import models.dto.AdminDTO;
import models.entities.Admin;
import models.entities.ProfilesCollection;
import play.data.Form;
import services.impl.ProfilesCollectionServiceImpl;


@ImplementedBy(ProfilesCollectionServiceImpl.class)
public interface ProfilesCollectionService {

    ProfilesCollection getProfilesCollectionByAdmin(Admin admin);

    void addProfilesCollection(Admin admin);

    AdminDTO createFromForm(Form<AdminDTO> form);
}
