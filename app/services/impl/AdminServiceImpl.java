package services.impl;

import dao.AdminDao;
import models.dto.AdminDTO;
import models.entities.Admin;
import org.apache.commons.codec.digest.DigestUtils;
import play.data.Form;
import services.AdminService;
import utils.Converter;


import javax.inject.Inject;

public class AdminServiceImpl implements AdminService {

    @Inject
    private AdminDao adminDao;

    @Override
    public boolean logIn(Admin admin) {
        String encodedPassword = DigestUtils.md5Hex(admin.getPassword());
        admin.setPassword(encodedPassword);
        return adminDao.isExist(admin);
    }

    @Override
    public boolean create(Admin admin) {
        String encodedPassword = DigestUtils.md5Hex(admin.getPassword());
        admin.setPassword(encodedPassword);
        return adminDao.registerAdmin(admin);
    }

    @Override
    public Admin getAdminByName(String name) {
        return adminDao.getAdminByName(name);
    }

    @Override
    public Admin getAdminById(int id) {
        return adminDao.get(id);
    }

    @Override
    public AdminDTO createFromForm(Form<AdminDTO> form) {
        if (form.hasErrors()) {
            return null;
        }
        AdminDTO adminDTO = form.get();
        boolean isAuthorized = logIn(Converter.getAdmin(adminDTO));
        if (!isAuthorized) {
            return null;
        }
        Admin admin = getAdminByName(adminDTO.getName());
        adminDTO.setPassword(null);
        adminDTO.setId(admin.getId());
        return adminDTO;
    }

}
