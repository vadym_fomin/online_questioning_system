package services.impl;

import builder.FieldBuilder;
import models.dto.FieldDTO;
import models.entities.Admin;
import models.entities.ProfilesCollection;
import play.data.DynamicForm;
import play.libs.Json;
import services.FieldFormHandler;
import services.ProfilesCollectionService;
import utils.Constant;

import javax.inject.Inject;
import java.util.Map;

/**
 * Created by Vadim on 13.12.2016.
 */
public class FieldFormHandlerImpl implements FieldFormHandler {

    @Inject
    private ProfilesCollectionService profilesCollectionService;

    @Override
    public FieldDTO handle(DynamicForm dynamicForm, Admin admin) {
        ProfilesCollection profilesCollection = profilesCollectionService.getProfilesCollectionByAdmin(admin);
        Map<String, String> requests = dynamicForm.data();
        requests.put(Constant.RESPONSE_COLLECTION, Json.toJson(profilesCollection).toString());
        dynamicForm = dynamicForm.fill(requests);
        FieldBuilder builder = new FieldBuilder(dynamicForm);
        FieldDTO fieldDTO = builder.getFieldDTOFromRequest();
        return fieldDTO;
    }
}
