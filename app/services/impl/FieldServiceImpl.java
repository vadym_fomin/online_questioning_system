package services.impl;

import java.util.Set;

import dao.FieldDao;
import models.dto.FieldDTO;
import models.entities.Field;
import models.entities.ProfilesCollection;
import services.FieldService;
import utils.Converter;
import validators.FieldValidator;
import validators.Validator;

import javax.inject.Inject;

/**
 * This class performs the manipulation of fields through a dao layer
 */
public class FieldServiceImpl implements FieldService {

    @Inject
    private FieldDao fieldDao;


    @Override
    public void delete(int id) {
        Field field = new Field();
        field.setId(id);
        fieldDao.delete(field);
    }

    @Override
    public Set<Field> getFieldsByProfilesCollection(ProfilesCollection profilesCollection) {
        return fieldDao.getFieldsByProfilesCollection(profilesCollection);
    }

    @Override
    public boolean add(FieldDTO fieldDTO) {
        Validator<FieldDTO> validator = new FieldValidator();
        if (!validator.validate(fieldDTO)) {
            return false;
        }
        Field field = Converter.getField(fieldDTO);
        fieldDao.insert(field);
        return true;
    }

    @Override
    public boolean edit(FieldDTO fieldDTO) {
        Validator<FieldDTO> validator = new FieldValidator();
        if (!validator.validate(fieldDTO)) {
            return false;
        }
        Field field = Converter.getField(fieldDTO);
        fieldDao.update(field);
        return true;
    }

    @Override
    public Field getById(int id) {
        return fieldDao.get(id);
    }

}
