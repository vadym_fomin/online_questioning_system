package services.impl;

import java.util.ArrayList;
import java.util.List;

import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import dao.FieldDao;
import dao.ProfileDao;
import models.entities.Field;
import models.entities.Profile;
import models.entities.ProfilesCollection;
import play.Logger;
import services.PDFService;
import utils.Constant;

import javax.inject.Inject;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;


public class PDFServiceImpl implements PDFService {

    private static final String HEADER_TEXT = "All responses";

    @Inject
    private ProfileDao profileDao;

    @Inject
    private FieldDao fieldDao;

    @Override
    public void createPDF(ProfilesCollection profilesCollection) {
        try {
            List<Field> fields = new ArrayList<>(fieldDao.getFieldsByProfilesCollection(profilesCollection));
            List<Profile> profiles = new ArrayList<>(profileDao.getProfilesByProfilesCollection(profilesCollection));
            PdfWriter writer = new PdfWriter(Constant.PDF_FILE);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf);
            Paragraph paragraph = new Paragraph(HEADER_TEXT);
            paragraph.setBold();
            document.add(paragraph);
            Table table = new Table(fields.size());
            addHeader(table, fields);
            addContent(table, profiles);
            document.add(table);
            document.close();
        } catch (Exception e) {
            Logger.error(e.getMessage());
        }
    }

    private void addHeader(Table table, List<Field> fields) {
        for (Field field : fields) {
            Cell cell = new Cell();
            cell.add(field.getLabel()).setBold();
            table.addHeaderCell(cell);
        }
    }

    private void addContent(Table table, List<Profile> profiles) {
        for (Profile profile : profiles) {
            for (String data : profile.getInputParametersList()) {
                Cell cell = new Cell();
                cell.add(data);
                table.addCell(cell);
            }
        }
    }


}
