package services.impl;

import builder.ProfileBuilder;
import models.dto.ProfileDTO;
import models.entities.Admin;
import models.entities.Field;
import models.entities.ProfilesCollection;
import services.FieldService;
import services.ProfileFormHandler;
import services.ProfileService;
import services.ProfilesCollectionService;
import utils.Converter;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Vadim on 13.12.2016.
 */
public class ProfileFormHandlerImpl implements ProfileFormHandler {

    @Inject
    private FieldService fieldService;

    @Inject
    private ProfilesCollectionService profilesCollectionService;


    @Override
    public ProfileDTO handle(Map<String, String[]> request, Admin admin) {
        ProfilesCollection profilesCollection = profilesCollectionService.getProfilesCollectionByAdmin(admin);
        List<Field> fields = new ArrayList<>(fieldService.getFieldsByProfilesCollection(profilesCollection));
        ProfileBuilder builder = new ProfileBuilder(request, fields, Converter.getProfilesCollectionDTO(profilesCollection));
        ProfileDTO profileDTO = builder.getProfileDTOFromRequest();
        return profileDTO;
    }
}
