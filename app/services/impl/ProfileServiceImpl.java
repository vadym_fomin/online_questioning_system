package services.impl;

import dao.FieldDao;
import dao.ProfileDao;
import models.dto.ProfileDTO;
import models.entities.Field;
import models.entities.Profile;
import models.entities.ProfilesCollection;
import services.ProfileService;
import utils.Converter;
import validators.ProfileValidator;
import validators.Validator;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class ProfileServiceImpl implements ProfileService {

    @Inject
    private ProfileDao profileDao;

    @Inject
    private FieldDao fieldDao;

    @Override
    public boolean add(ProfileDTO profileDTO) {
        Profile profile = Converter.getProfile(profileDTO);
        ProfilesCollection profilesCollection = profile.getProfilesCollection();
        List<Field> fields = new ArrayList<>(fieldDao.getFieldsByProfilesCollection(profilesCollection));
        Validator<ProfileDTO> validator = new ProfileValidator(fields);
        if (!validator.validate(profileDTO)) {
            return false;
        }
        profileDao.insert(profile);
        return true;
    }

    @Override
    public Set<Profile> getProfilesByProfilesCollection(ProfilesCollection profilesCollection) {
        return profileDao.getProfilesByProfilesCollection(profilesCollection);
    }

    @Override
    public long getProfilesCount(ProfilesCollection profilesCollection) {
        return profileDao.getProfilesCountInProfilesCollection(profilesCollection);
    }
}
