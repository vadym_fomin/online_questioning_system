package services.impl;

import dao.ProfilesCollectionDao;
import models.dto.AdminDTO;
import models.entities.Admin;
import models.entities.ProfilesCollection;
import play.data.Form;
import services.AdminService;
import services.ProfilesCollectionService;
import utils.Converter;

import javax.inject.Inject;


public class ProfilesCollectionServiceImpl implements ProfilesCollectionService {

    @Inject
    private ProfilesCollectionDao profilesCollectionDao;

    @Inject
    private AdminService adminService;

    @Override
    public ProfilesCollection getProfilesCollectionByAdmin(Admin admin) {
        ProfilesCollection profilesCollection = profilesCollectionDao.getProfilesCollectionByAdmin(admin);
        return profilesCollection;
    }

    @Override
    public void addProfilesCollection(Admin admin) {
        ProfilesCollection profilesCollection = new ProfilesCollection();
        profilesCollection.setAdmin(admin);
        profilesCollectionDao.insert(profilesCollection);

    }

    /**
     * This method creates profiles collection from hhtp form
     *
     * @return admin associated with this profiles collection
     **/
    @Override
    public AdminDTO createFromForm(Form<AdminDTO> form) {
        if (form.hasErrors()) {
            return null;
        }
        AdminDTO adminDTO = form.get();
        boolean isCreated = adminService.create(Converter.getAdmin(adminDTO));
        if (!isCreated) {
            return null;
        }
        Admin admin = adminService.getAdminByName(adminDTO.getName());
        addProfilesCollection(admin);
        adminDTO.setPassword(null);
        adminDTO.setId(admin.getId());
        return adminDTO;
    }
}
