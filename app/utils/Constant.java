package utils;

/**
 * This class contains a set of constants used in the program
 */
public class Constant {

    public static final String CONFIG = "config";

    public static final String EMPTY_VALUE = "N/A";

    public static final String SESSION_PARAMETER = "name";

    public static final String PDF_FILE = "table.pdf";

    public static final String MAIN_PAGE = "/test.com/main";

    public static final String ALL_FILEDS_PAGE = "/test.com/fields";

    public static final String ID = "id";

    public static final String LABEL = "label";

    public static final String TYPE = "type";

    public static final String TEXT = "text";

    public static final String MIN = "min";

    public static final String MAX = "max";

    public static final String REAL_TYPE = "realType";

    public static final String IMPORTANCE = "importance";

    public static final String EXTREMUM = "extremum";

    public static final String REQUIRED = "required";

    public static final String ACTIVE = "active";

    public static final String RESPONSE_COLLECTION = "responseCollection";

    public static final String SPACE = " ";

    public static final String EMPTY_STRING = "";

    public static final String ADMIN_ID = "adminId";

    public static final String CONTENT_TYPE = "application/pdf";
}
