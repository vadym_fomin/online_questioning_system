package utils;

import com.fasterxml.jackson.databind.JsonNode;

import models.dto.AdminDTO;
import models.dto.FieldDTO;
import models.dto.ProfileDTO;
import models.dto.ProfilesCollectionDTO;
import models.entities.Admin;
import models.entities.Field;
import models.entities.Profile;
import models.entities.ProfilesCollection;
import org.hibernate.Hibernate;
import play.Logger;
import play.libs.Json;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This class implements methods for converting entities in dto and vice versa
 */
public class Converter {

    public static AdminDTO getAdminDTO(Admin admin) {
        JsonNode adminJson = Json.toJson(admin);
        return Json.fromJson(adminJson, AdminDTO.class);
    }

    public static Admin getAdmin(AdminDTO adminDTO) {
        JsonNode adminJson = Json.toJson(adminDTO);
        return Json.fromJson(adminJson, Admin.class);
    }

    public static ProfilesCollectionDTO getProfilesCollectionDTO(ProfilesCollection profilesCollection) {
        JsonNode profilesCollectionJson = Json.toJson(profilesCollection);
        return Json.fromJson(profilesCollectionJson, ProfilesCollectionDTO.class);
    }

    public static ProfilesCollection getProfilesCollection(ProfilesCollectionDTO profilesCollectionDTO) {
        JsonNode profilesCollectionJson = Json.toJson(profilesCollectionDTO);
        return Json.fromJson(profilesCollectionJson, ProfilesCollection.class);
    }

    public static ProfileDTO getProfileDTO(Profile profile) {
        JsonNode profileJson = Json.toJson(profile);
        ProfileDTO dto = Json.fromJson(profileJson, ProfileDTO.class);
        dto.setProfilesCollectionDTO(getProfilesCollectionDTO(profile.getProfilesCollection()));
        return dto;
    }

    public static Profile getProfile(ProfileDTO profileDTO) {
        JsonNode profileJson = Json.toJson(profileDTO);
        Profile profile = Json.fromJson(profileJson, Profile.class);
        profile.setProfilesCollection(getProfilesCollection(profileDTO.getProfilesCollectionDTO()));
        return profile;
    }

    public static FieldDTO getFieldDTO(Field field) {
        JsonNode fieldJson = Json.toJson(field);
        FieldDTO dto = Json.fromJson(fieldJson, FieldDTO.class);
        dto.setProfilesCollectionDTO(getProfilesCollectionDTO(field.getProfilesCollection()));
        return dto;
    }

    public static Field getField(FieldDTO fieldDTO) {
        JsonNode fieldJson = Json.toJson(fieldDTO);
        Field field = Json.fromJson(fieldJson, Field.class);
        field.setProfilesCollection(getProfilesCollection(fieldDTO.getProfilesCollectionDTO()));
        return field;
    }

    public static Collection<ProfileDTO> getProfilesDTO(Collection<Profile> profiles) {
        Collection<ProfileDTO> profilesDTO = new ArrayList<>();
        for (Profile profile : profiles) {
            profilesDTO.add(getProfileDTO(profile));
        }
        return profilesDTO;
    }

    public static Collection<Profile> getProfiles(Collection<ProfileDTO> profilesDTO) {
        Collection<Profile> profiles = new ArrayList<>();
        for (ProfileDTO profileDTO : profilesDTO) {
            profiles.add(getProfile(profileDTO));
        }
        return profiles;
    }

    public static Collection<FieldDTO> getFieldsDTO(Collection<Field> fields) {
        Collection<FieldDTO> fieldsDTO = new ArrayList<>();
        if (fields != null) {
            for (Field field : fields) {
                fieldsDTO.add(getFieldDTO(field));
            }
        }
        return fieldsDTO;
    }

    public static Collection<Field> getFields(Collection<FieldDTO> fieldsDTO) {
        Collection<Field> fields = new ArrayList<>();
        if (fieldsDTO != null) {
            for (FieldDTO fieldDTO : fieldsDTO) {
                fields.add(getField(fieldDTO));
            }
        }
        return fields;
    }

}
