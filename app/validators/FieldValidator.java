package validators;

import models.dto.FieldDTO;
import models.dto.FieldTypeDTO;

/**
 * This class is designed to validate the  FieldDTO class
 */
public class FieldValidator implements Validator<FieldDTO> {

    private static final int MIN_VALUE_INDEX = 0;

    private static final int MAX_VALUE_INDEX = 1;


    @Override
    public boolean validate(FieldDTO field) {
        if (field.getLabel() == null || field.getLabel().isEmpty()) {
            return false;
        }
        if (field.getValuesList() != null) {
            if (field.getValuesList().isEmpty()) {
                return false;
            }
            if (field.getFieldType() == FieldTypeDTO.SLIDER) {
                if (field.getValuesList().get(MIN_VALUE_INDEX).getFieldValue().isEmpty() ||
                        field.getValuesList().get(MAX_VALUE_INDEX).getFieldValue().isEmpty()) {
                    return false;
                } else if (Integer.parseInt(field.getValuesList().get(MIN_VALUE_INDEX).getFieldValue()) >= Integer
                        .parseInt(field.getValuesList().get(MAX_VALUE_INDEX).getFieldValue())) {
                    return false;
                }
            }
        }
        return true;
    }

}
