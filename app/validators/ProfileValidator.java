package validators;

import java.util.List;

import models.dto.ProfileDTO;
import models.entities.Field;

import static utils.Constant.EMPTY_VALUE;

/**
 * This class is designed to validate the ProfileDTO class
 */
public class ProfileValidator implements Validator<ProfileDTO> {

    private List<Field> fields;

    public ProfileValidator(List<Field> fields) {
        this.fields = fields;
    }

    @Override
    public boolean validate(ProfileDTO profile) {
        List<String> profileValues = profile.getInputParametersList();
        for (int i = 0; i < fields.size(); i++) {
            if (fields.get(i).isRequired() && EMPTY_VALUE.equals(profileValues.get(i))) {
                return false;
            }
        }
        return true;
    }

}
