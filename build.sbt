import sbt._
import Process._
import Keys._

lazy val root = (project in file(".")).
  settings(
    organization := "edu.coursework",
    name := "dataCollectionService",
    version := "1.0",
    scalaVersion := "2.11.6"
  ).
  enablePlugins(PlayJava, QueryDSLPlugin)

javacOptions += "-g:none"
queryDSLPackage := "src/main/java/coursework/models/entities"
javacOptions ++= Seq("-source", "1.8")
javaOptions := Seq("-Dconfig.file=conf/application.conf")
javaOptions in Test := Seq("-Dconfig.file=conf/test.conf")

libraryDependencies ++= Seq(
  javaJdbc,
  javaJpa,
  cache,
  javaWs,
  "org.postgresql" % "postgresql" % "9.4.1208",
  "com.mysema.querydsl" % "querydsl-apt" % "3.6.2",
  "org.hibernate" % "hibernate-entitymanager" % "5.1.0.Final",
  "junit" % "junit" % "4.12",
  "org.mockito" % "mockito-core" % "1.8.5",
  "org.powermock" % "powermock-module-junit4" % "1.6.6",
  "org.powermock" % "powermock-api-mockito" % "1.6.6",
  "com.itextpdf" % "pdfa" % "7.0.0",
  "com.itextpdf" % "layout" % "7.0.0"

)

routesGenerator := InjectedRoutesGenerator
