/*
 It performs ajax request on AdminActionsController.addField
 action method
 */
$("#add").submit(function (e) {
    e.preventDefault();
    $.ajax({
        url: "http://localhost:9000/test.com/save", data: $("form").serialize(), type: "POST",
        success: function () {
            window.location = "http://localhost:9000/test.com/fields";
        }, error: function () {
            alert("Required fields not fills or error in slider type filling");
        }
    });
});