 $('#responseForm').submit(function (e) {
    e.preventDefault();
    if (validateUserForm()) {
        var id = $('#adminId').text();
        $.post('/test.com/addProfile/' + id, 
                $('#responseForm').serialize(),
                function () {
                     window.location = '/test.com/main';
                });
    }
    else {
        alert('Missing a required field value');
    }
});
