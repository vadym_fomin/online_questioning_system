 $(document).ready(function () {
    $('#logIn').submit(function (e) {
        e.preventDefault();
        if (validate('logIn')) {
            $.ajax({
                url: '/test.com/logIn',
                data: $('#logInForm').serialize(),
                type: 'POST',
                success: function () {
                    window.location = '/test.com/fields';
                },
                error: function () {
                    error = document.getElementById('error1');
                    error.innerHTML = 'User not found';
                    error.style.color = 'red';
                    error.style['display'] = 'block';
                }
            });
        }
    });

    $('#signUp').submit(function (e) {
        e.preventDefault();
        if(validate('signUp')){
            $.ajax({
                url: '/test.com/addProfilesCollection',
                data: $('#signUpForm').serialize(),
                type: 'POST',
                success: function () {
                    window.location = '/test.com/fields';
                },
                error: function () {
                    error = document.getElementById('error2');
                    error.innerHTML = 'User with the same login already exists';
                    error.style.color = 'red';
                    error.style['display'] = 'block';
                }
            });
        }
    });
    
    $(".hideValue").bind("click", function(e){
        $("#name1, #name2, #password1, #password2").val("");
        $("#error1, #error2").css("display","none");
    });
   //$(".footer")[1].remove();
});
