 $(document).ready(function () { 
               
    $(".delete").click(function () {
        var rowId = $(this).parent().parent().attr("id");
        $("#fieldId").text(rowId);
        $("#myModal").modal();
    });

    /*
     It performs ajax request on AdminActionsController.deleteField
     action method and hide row with this field
     */
    $("#ok").click(function () {
        $("#myModal").modal("toggle");
        var rowId = $("#fieldId").text();
        $.post("/test.com/delete",
            {id: rowId},
            function () {
                $("#" + rowId).hide();
            });
    });
});