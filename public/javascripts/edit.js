/*
 It performs ajax request on AdminActionsController.editField
 action method
 */
$("#edit").submit(function (e) {
    e.preventDefault();
    $.ajax({
        url: "/test.com/update", data: $("form").serialize(), type: "POST",
        success: function () {
            window.location = "http://localhost:9000/test.com/fields";
        }, error: function () {
            alert("Required fields not fills or error in slider type filling");
        }
    });
});