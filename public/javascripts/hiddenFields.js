function show(option) {
    var type = document.forms['edit']['option'].value;
    if (type == 'radio button' || type == 'check box' || type == 'combo box' || type == 'slider') {
        document.getElementById('label').style.display = option;
        if (type == 'radio button' || type == 'check box' || type == 'combo box') {
            document.getElementById('textarea').style.display = option;
            document.getElementById('minValue').style.display = 'none';
            document.getElementById('maxValue').style.display = 'none';
        }
        if (type == 'slider') {
            document.getElementById('minValue').style.display = option;
            document.getElementById('maxValue').style.display = option;
            document.getElementById('textarea').style.display = 'none';
        }
    } else {
        document.getElementById('label').style.display = 'none';
        document.getElementById('textarea').style.display = 'none';
        document.getElementById('minValue').style.display = 'none';
        document.getElementById('maxValue').style.display = 'none';
    }
}

function showStatistics() {
    var realType = document.forms['edit']['typeOption'].value;
    var option = 'none';
    if (realType == 'number') {
        option = 'block';
    }
    document.getElementById('imp').style.display = option;
    document.getElementById('extr').style.display = option;
    document.getElementById('slider').style.display = option;
    document.getElementById('extrOption').style.display = option;



}
