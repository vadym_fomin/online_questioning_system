$("#order").click(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'GET',
        url: '/test.com/orderedProfiles',
        dataType: 'json',
        async: true,
        success: function (profiles) {
            $("tbody").remove();
            var table = document.getElementsByTagName("table")[0];
            var body = document.createElement("tbody");

            for (var i = 0; i < profiles.length; i++) {
                var row = document.createElement("tr");
                for (var j = 0; j < profiles[i].inputParametersList.length; j++) {
                    var cell = document.createElement("td");
                    cell.appendChild(document.createTextNode(profiles[i].inputParametersList[j]));
                    row.appendChild(cell);
                }
                body.appendChild(row);
            }
            table.appendChild(body);
        }
    });
});
