function validate(formName) {
    var name = document.forms[formName]['name'].value;
    var pass = document.forms[formName]['password'].value;
    var namePattern = /^[a-zA-Z]{5,15}$/g;
    var passPattern = /^[a-zA-Z][a-zA-Z0-9-_\.]{5,19}$/g;
    if (namePattern.test(name) == false || passPattern.test(pass) == false) {
        if (formName=='logIn') {
            error = document.getElementById('error1');
        }
        else {
            error = document.getElementById('error2');
        }
        error.innerHTML = 'Invalid name or password';
        error.style.display = 'block';
        error.style.color = 'red';
        return false;
    }
    return true;
}

function validateUserForm() {
    var mass = $('.required');
    for (i = 0; i < mass.length; i++) {
        if (mass[i].tagName.toLowerCase() == 'div') {
            var childs = mass[i].children;
            var chosen = false;
            for (j = 0; j < childs.length; j++) {
                var inputs = childs[j].children
                for (k = 0; k < inputs.length; k++) {
                    if (inputs[k].checked) {
                        chosen = true;
                        break;
                    }
                }
            }
            return chosen;
        } else {
            if (mass[i].value == null || mass[i].value == '') {
                return false;
            }
        }
    }
    return true;
}
